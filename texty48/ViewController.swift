//
//  ViewController.swift
//  TEXTY48
//
//  Created by Jasper Andrew on 2/7/17.
//  Copyright © 2017 Jasper Andrew. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var the_board_view: UITextView!
    @IBOutlet var score_label: UILabel!
    @IBOutlet var message_label: UILabel!
    
    @IBAction func swipeU(_ sender: UISwipeGestureRecognizer) {
        if(!gameover) { move(direction: "up") }
        else {
            reset()
            
            insertTile()
            insertTile()
            insertTile()
            
            self.the_board_view.text = printTheBoard()
            self.score_label.text = String(getScore())
            self.message_label.text = ""
        }
    }
    
    @IBAction func swipeL(_ sender: UISwipeGestureRecognizer) {
        if(!gameover) { move(direction: "lt") }
    }
    
    @IBAction func swipeD(_ sender: UISwipeGestureRecognizer) {
        if(!gameover) { move(direction: "dn") }
    }
    
    @IBAction func swipeR(_ sender: UISwipeGestureRecognizer) {
        if(!gameover) { move(direction: "rt") }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        insertTile()
        insertTile()
        insertTile()
        
        self.the_board_view.text = printTheBoard()
        self.score_label.text = String(getScore())
        self.message_label.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func move(direction:String) {
        if pushTiles(direc: direction) { insertTile() }

        self.the_board_view.text = printTheBoard()
        self.score_label.text = String(getScore())
        
        if(num_empties == 0 && gameIsOver()){
            gameover = true
            self.the_board_view.text =  ".---------------.\n" +
                                        "|   |   |   |   |\n" +
                                        "|---------------|\n" +
                                        "| G | A | M | E |\n" +
                                        "|---------------|\n" +
                                        "| O | V | E | R |\n" +
                                        "|---------------|\n" +
                                        "|   |   |   |   |\n" +
                                        "'---------------'"
            self.message_label.text = "Swipe up to restart"
        }
    }
    
    func test() {
        for family in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
}

