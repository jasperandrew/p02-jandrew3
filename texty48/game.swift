//
//  main.swift
//  TEXTY48
//
//  Created by Jasper Andrew on 2/2/17.
//  Copyright © 2017 Jasper Andrew. All rights reserved.
//

import Foundation

// Prints the board.
func printTheBoard()->String {
	var board_str = ".---------------.\n"
	for i in 0..<4 {
		
		for j in 0..<4 {
			let n = board[i][j]
			board_str += "| \(sym[n]!) "
		}
		board_str += "|\n"
        if i < 3 {board_str += "|---------------|\n"}
	}
	board_str += "'---------------'\n"
    return board_str
}

// Checks if a given tile can move in the given direction.
// Returns 0 if not, 1 if yes, and 2 if it can merge with the adjacent tile.
func tileCanMove(row:Int, col:Int, direc:String)->Int {
	
	switch direc {
		
	case "up":
		if row == 0 {return 0}
		if board[row-1][col] == 0 {return 1}
		if board[row-1][col] == board[row][col] {return 2}
		return 0
		
	case "dn":
		if row == 3 {return 0}
		if board[row+1][col] == 0 {return 1}
		if board[row+1][col] == board[row][col] {return 2}
		return 0
		
	case "lt":
		if col == 0 {return 0}
		if board[row][col-1] == 0 {return 1}
		if board[row][col-1] == board[row][col] {return 2}
		return 0
		
	case "rt":
		if col == 3 {return 0}
		if board[row][col+1] == 0 {return 1}
		if board[row][col+1] == board[row][col] {return 2}
		return 0
		
	default:
		exit(1)
	}
}

// Moves or merges a given tile in the given direction.
func moveTile(row:Int, col:Int, direc:String, val:Int) {
	
	switch direc {
		
	case "up":
		board[row-1][col] = val*board[row][col]
		empty_tiles[(row-1)*4+col] = 0
		break
		
	case "dn":
		board[row+1][col] = val*board[row][col]
		empty_tiles[(row+1)*4+col] = 0
		break
		
	case "lt":
		board[row][col-1] = val*board[row][col]
		empty_tiles[row*4+(col-1)] = 0
		break
		
	case "rt":
		board[row][col+1] = val*board[row][col]
		empty_tiles[row*4+(col+1)] = 0
		break
		
	default:
		exit(1)
	}
	
	board[row][col] = 0
	empty_tiles[row*4+col] = 1
}

// Pushes all tiles in a given direction.
func pushTiles(direc:String)->Bool {
	
	let uplt = ["up","lt"].contains(direc) ? true : false
    
	var moved = false
	
    var cont = true
	while cont {
		
		cont = false
		var i = uplt ? 0 : 3
		while i != -1 && i != 4 {
			
			var j = uplt ? 0 : 3
			while j != -1 && j != 4 {
				
				if board[i][j] != 0 {
					let canmove = tileCanMove(row: i, col: j, direc: direc)
					if canmove == 1 {
                        moved = true
						cont = true
						moveTile(row: i, col: j, direc: direc, val: 1)
					}
					if canmove == 2 {
                        moved = true
						cont = true
						moveTile(row: i, col: j, direc: direc, val: 2)
						num_empties += 1
					}
				}
				
				if uplt {j += 1} else {j -= 1}
			}
			
			if uplt {i += 1} else {i -= 1}
		}
	}
    return moved
}

// Selects a random tile from the set of empty tiles.
func getRandEmptyTile()->Int {
	
	var idx = Int(arc4random_uniform(UInt32(num_empties)))
	for i in 0..<16 {
		
		if(empty_tiles[i] == 1){
			if idx == 0 {return i}
			idx -= 1
		}
	}
	return -1
}

// Inserts a new tile into the board. 
// If no value is given, it will be 1 or 2.
// If no location is given, it will be a random empty location.
func insertTile(val:Int = -1, row:Int = -1, col:Int = -1) {
	
	let value = (val == -1 ? Int(arc4random_uniform(2)+1) : val)
	if row == -1 || col == -1 {
		let loc = getRandEmptyTile()
		if loc == -1 {return}
		
		let c = loc % 4
		let r = (loc - c)/4
		
		board[r][c] = value
		empty_tiles[loc] = 0
		
	} else {
		board[row][col] = value
	}
	
	num_empties -= 1
}

// Checks if game is over. 
// Returns result.
func gameIsOver()->Bool {
    for i in 0..<4 {
        for j in 0..<4 {
            for direc in ["up","lt","dn","rt"] {
                let moveable = tileCanMove(row: i, col: j, direc: direc)
                if moveable == 1 || moveable == 2 {return false}
            }
        }
    }
	return true;
}

// Resets the board and other variables
func reset() {
    board = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    empty_tiles = [1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1]
    num_empties = 16
    gameover = false
}

// Returns the score of the current board
func getScore()->Int {
    var score = 0
    for i in 0..<4 {
        for j in 0..<4 {
            score += board[i][j]
        }
    }
    return score
}
