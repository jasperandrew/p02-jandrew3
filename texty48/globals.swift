//
//  globals.swift
//  TEXTY48
//
//  Created by Jasper Andrew on 2/7/17.
//  Copyright © 2017 Jasper Andrew. All rights reserved.
//


// The 2D array storing the board's values.
var board = [[0,0,0,0],
             [0,0,0,0],
             [0,0,0,0],
             [0,0,0,0]]


// Keeping track of empty tiles
var empty_tiles = [1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1]
var num_empties = 16


// A dictionary holding the text equivalents of each number
var sym:[Int:String] = [0:" ", 1:"1", 2:"2", 4:"4", 8:"8",
                        16:"A", 32:"B", 64:"C", 128:"D", 256:"E",
                        512:"F", 1024:"G", 2048:"H", 4096:"I", 8192:"J"]


// The state of the game
var gameover = false
